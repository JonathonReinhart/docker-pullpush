#!/usr/bin/env python3
import subprocess

def docker_cmd(*args, allow_fail=False):
    args = ['docker'] + list(args)
    print("+ " + " ".join(args))
    try:
        subprocess.check_call(args)
    except subprocess.CalledProcessError:
        if not allow_fail:
            raise

def docker_pull(img):
    docker_cmd('pull', img)

def docker_push(img):
    docker_cmd('push', img)

def docker_tag(source, target):
    docker_cmd('tag', source, target)

def docker_rmi(*images, allow_fail=False):
    docker_cmd('rmi', *images, allow_fail=allow_fail)


class Imgspec:
    def __init__(self, args, imgname):
        self.args = args
        self.imgname = imgname

    @property
    def old(self):
        return self.args.old_reg + '/' + self.imgname

    @property
    def new(self):
        return self.args.new_reg + '/' + self.imgname

    def __repr__(self):
        return "Imgspec({!r})".format(self.imgname)


def parse_args():
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('--old-reg', required=True,
            help="Old registry base URL")
    ap.add_argument('--new-reg', required=True,
            help="New registry base URL")
    ap.add_argument('images', nargs="+",
            help="Images to pull from old registry and push to new registry en masse")

    args = ap.parse_args()

    for i in range(len(args.images)):
        args.images[i] = Imgspec(args, args.images[i])

    return args


def main():
    args = parse_args()

    cleanup = []

    try:
        for img in args.images:
            print("\n{} -> {}".format(img.old, img.new))

            docker_pull(img.old)
            cleanup.append(img)

            docker_tag(img.old, img.new)

            docker_push(img.new)

    finally:
        print("\nCleaning up...")
        for img in cleanup:
            docker_rmi(img.old, img.new)


if __name__ == '__main__':
    main()
